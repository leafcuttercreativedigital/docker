ARG BASE_IMAGE=leafcutter/php-node-base:latest
FROM $BASE_IMAGE

MAINTAINER hello@leafcutter.com.au

# install needed libraries
RUN apt-get update \
  && apt-get install -y --force-yes \
    git \
    zlib1g-dev \
    libmcrypt-dev \
    libpng-dev \
    libxml2-dev \
    libzip-dev \
    libcurl4-openssl-dev \
    gnupg \
    rsync \
    zip \
    vim \
    mariadb-client \
    python \
    python-pip \
    curl \
    unzip \
    groff \
  && rm -rf /var/lib/apt/lists/*

# install needed extensions
## update 2018/05 mcrypt deprecated on php 7.2
ARG EXTRA_DEPS=''
RUN docker-php-ext-install mysqli pdo pdo_mysql zip soap curl gd $EXTRA_DEPS \
  && docker-php-ext-enable mysqli pdo pdo_mysql zip soap curl gd $EXTRA_DEPS 

# install chrome
RUN echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google-chrome.list \
 && curl https://dl.google.com/linux/linux_signing_key.pub | apt-key add - \
 && apt update \
 && apt install -y google-chrome-stable \
 && rm -rf /var/lib/apt/lists/*

# install composer latest
RUN curl -sS https://getcomposer.org/installer \
  | php -- --install-dir=/usr/local/bin --filename=composer \
  && composer -V

# install node 8 npm 5
RUN curl -sL https://deb.nodesource.com/setup_10.x \
  | bash - \
  && apt-get install -y nodejs \
  && rm -rf /var/lib/apt/lists/* \
  && rm /etc/apt/sources.list.d/nodesource.list

# install some base node tools
RUN npm i -g \
    gulp \
    bower \
    testcafe

# install libsass globally, this will prevent compiling native binaries every time
# an app needs node-sass
# @see https://hub.docker.com/r/kazu69/node/~/dockerfile/
RUN npm install --unsafe-perm -g node-sass \
    && touch $HOME/.npmrc \
    && echo "sass_binary_cache=/usr/lib/node_modules/node-sass/vendor/linux-x64-64/binding.node" >> $HOME/.npmrc

ENV SASS_BINARY_PATH /usr/lib/node_modules/node-sass/vendor/linux-x64-64/binding.node

# install wp-cli
RUN curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar \
    && chmod +x wp-cli.phar \
    && mv wp-cli.phar /usr/local/bin/wp

# Add wp-cli packages here
RUN php -d memory_limit=-1 "$(which wp)" --allow-root \
  package install https://github.com/wpbullet/wp-menu-import-export-cli.git

# set up shopify theme client
RUN curl -s https://shopify.github.io/themekit/scripts/install.py | python

# set up amazon tools
RUN pip install awsebcli pyrsistent==0.16.0 \
	&& curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" \
	&& unzip awscliv2.zip \
	&& ./aws/install

# docker-compose
RUN curl -L https://github.com/docker/compose/releases/download/1.13.0/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose

# confluence-cli
RUN apt update \
 && apt install -y golang \
 && rm -rf /var/lib/apt/lists/* \
	&& go get github.com/proctorlabs/confluence-cli
ENV PATH="${PATH}:/root/go/bin"

# set up lighthouse and broken links checker for website performance testing
RUN npm install -g \
    lighthouse \
    broken-link-checker

# set up default apache server
RUN mkdir -p /app/public \
    && chmod 777 /app/public

RUN a2enmod rewrite

COPY apache.conf /etc/apache2/sites-available/000-default.conf

# copy the latest leafcutter command build
# COPY code-review-package.phar /usr/local/bin/leafcutter

# set up some aliases
RUN echo 'alias wp="wp --allow-root"' >> ~/.bashrc

# copy utility commands
COPY bin/* /usr/local/bin/

WORKDIR /app/public
