# Leafcutter base docker image

This image is used by automated builds.

__Do not use it in production__! This image is for development purposes only.

## Building / updating

To update the current image, run `./build.sh vX.Y.Z`. Make sure you are logged in
to docker hub via `docker login` before you run that command.

Optionally, a  `Makefile` is also provided when building without code-review-package updates.

In most cases, this is enough:
```shell
docker login
./build.sh
```

## TODO

- Add this repo to an automated build. Bitbucket integration wasn't working at the
time this repo was set up, but it may have been fixed since.
