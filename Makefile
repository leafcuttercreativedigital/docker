tag := leafcutter/php-node-base:latest
base_image := php:7-apache

latest: Dockerfile apache.conf
	docker build . -t $(tag) --pull --build-arg BASE_IMAGE=$(base_image)
	docker push $(tag)

build: Dockerfile apache.conf
	docker build . -t $(tag) --pull --build-arg BASE_IMAGE=$(base_image)

7.4:
	$(eval TAG=leafcutter/php-node-base:7.4)
	$(eval IMAGE=php:7.4-apache)
	docker build . -t $(TAG) --pull --build-arg BASE_IMAGE=$(IMAGE) --build-arg EXTRA_DEPS=filter
	docker push $(TAG)

7.3:
	$(eval TAG=leafcutter/php-node-base:7.3)
	$(eval IMAGE=php:7.3-apache)
	docker build . -t $(TAG) --pull --build-arg BASE_IMAGE=$(IMAGE)
	docker push $(TAG)

.make-local: composer.lock
	composer install
	touch .make-local

.make-submodules:
	git submodule init
	touch .make-submodules

.make-gitupdates:
	git submodule update --recursive --remote

.make-package-install: .make-submodules .make-gitupdates
	cd code-review-package && composer install
	touch .make-package-install

code-review-package.phar: .make-package-install .make-local
	php -d phar.readonly=off ./vendor/bin/phar-composer build code-review-package

clean:
	rm .make-*
