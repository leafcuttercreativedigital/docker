#!/bin/bash -ex

if [ ! -d vendor ]
then
	composer install
fi

if [ ! -d code-review-package ]
then
	git submodule init
fi

git submodule update --recursive --remote

pushd code-review-package
composer install
popd

./vendor/bin/phar-composer build code-review-package

base_tag="leafcutter/php-node-base"
latest="$base_tag:latest"

if [ ! -z "$1" ]
then
  tag="$base_tag:$1"
  docker build . -t $tag
  docker push $tag
fi

docker build . -t $latest
docker push $latest
